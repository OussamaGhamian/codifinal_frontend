export { default as Car } from './car.svg';
export { default as Confirm } from './confirm.svg';
export { default as Docs } from './docs.svg';
export { default as Medicine } from './medicine.svg';
export { default as Welcome } from './welcome.svg'
export { default as Login } from './login.svg'
export { default as Message } from './message.svg'
export { default as ME } from './me.svg'