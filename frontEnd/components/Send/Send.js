import React, { useState } from 'react'
import { View, Dimensions, Text } from 'react-native'
import Input from '../Input/Input'
import Icon from 'react-native-vector-icons/MaterialIcons'
import * as Animatable from 'react-native-animatable'
import 'react-native-get-random-values';
import { uuid } from 'uuidv4'

export default function Send({ addOffer, addRequest }) {
  const [isHidden, setHidden] = useState(true)
  const [content, setContent] = useState('')
  return (
    <>
      {!isHidden ?
        <Animatable.View animation='fadeInLeftBig' duration={500} style={{ alignItems: 'center', justifyContent: "space-around", flexDirection: 'row', borderWidth: 2, borderColor: '#10153B', width: Dimensions.get('window').width, borderTopRightRadius: 40, borderTopLeftRadius: 40, position: 'absolute', bottom: 0, backgroundColor: 'white', }}>
          <Input
            placeholder="اكتب هنا ما تريد بين 20 و 110 حروف"
            onChangeText={val => setContent(val)} />
          {content.length < 20 ?
            <Animatable.View animation="fadeInLeftBig" duration={500} style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 200, width: 50, height: 50, backgroundColor: '#10153B', margin: 5, borderColor: "#F9A826", borderWidth: 2 }} onTouchStart={() => setHidden(true)} >
              <Icon name='close' size={40} color="#F9A826" />
            </Animatable.View> :
            <Icon name="send" size={40} color="#F9A826"
              onPress={addOffer != undefined ?
                () => addOffer({
                  id: uuid(),
                  userName: 'فادي الحميد',
                  date: '02-02-2020',
                  phoneNbr: 76664873,
                  content
                }, setContent(''), setHidden(true))
                :
                () => addRequest({
                  id: '3',
                  userName: 'فادي الحميد',
                  date: '02-02-2020',
                  phoneNbr: 76664873,
                  content
                }, setContent(''), setHidden(true))} />}
        </Animatable.View>
        :
        <Animatable.View animation="fadeInRightBig" duration={500} style={{ justifyContent: 'center', alignItems: 'center', borderRadius: 200, width: 50, height: 50, backgroundColor: '#10153B', margin: 5, position: 'absolute', bottom: 0, left: 0, borderColor: "#F9A826", borderWidth: 2 }} onTouchStart={() => setHidden(false)} >
          <Icon name='add' size={40} color="#F9A826" />
        </Animatable.View>
      }
    </>

  )
}
