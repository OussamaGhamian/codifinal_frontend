import React, { useState } from 'react'
import { View, StyleSheet, Dimensions, Text, ToastAndroid } from "react-native";
import Icon from 'react-native-vector-icons/MaterialIcons'
import Ripple from 'react-native-material-ripple'
import DialModal from '../DialModal/DialModal'
import AddReview from '../AddReview/AddReview'
import UserAvatar from 'react-native-user-avatar';
import Input from '../Input/Input';


const Post = ({ date, userName, content, phoneNbr, show = true, del, postID, update }) => {
  const [editMode, setEditMode] = useState(false)
  const styles = StyleSheet.create({
    postContainer: {
      width: Dimensions.get('window').width - 40,
      minHeight: Dimensions.get('window').height / 4,
      justifyContent: 'center',
      marginVertical: 20,
      borderWidth: 1,
      borderRadius: 2,
      borderColor: '#eee',
      borderBottomWidth: 0,
      shadowColor: '#fff',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: .1,
      borderBottomEndRadius: 15,
      borderBottomStartRadius: 15,
      elevation: .8,
    }
    , header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 15
    },
    imgPost: {
      borderRadius: 100 / 2,
      width: 50,
      height: 50,
      backgroundColor: '#10153B',
      borderColor: '#F9A826',
      borderWidth: 2.5,
    },
    userNamePost: {
      marginLeft: 'auto',
      fontSize: 20,
      fontWeight: 'bold',
      paddingRight: 10,
      color: "#10153B"
    },
    datePost: {
      fontWeight: 'bold',
      color: '#F9A826',
    },
    main: {
      marginVertical: 5,
      fontSize: 18,
      lineHeight: 28,
      textAlign: 'right',
      paddingLeft: 15,
      paddingRight: 15,
      paddingTop: 15
    }, footer: {
      flexDirection: 'row',
      padding: 2,
      justifyContent: 'space-around',
      marginTop: 'auto',
      borderBottomEndRadius: 15,
      borderBottomStartRadius: 15,
      backgroundColor: '#10153B'
    },
    ripple: {
      width: Dimensions.get('screen').width / 2,
      justifyContent: 'center',
      alignItems: 'center',
      marginVertical: 2
    }
  })
  const [showDial, setShowDial] = useState(false)
  const [reviewMode, setreviewMode] = useState(false)
  const [newContent, setNewContent] = useState('')
  return (
    <View style={styles.postContainer}>
      {showDial ? <DialModal phoneNbr={phoneNbr} /> : null}
      {reviewMode ? <AddReview close={setreviewMode} /> : null}
      <View style={styles.header}>
        <Text style={styles.datePost}>{date}</Text>
        <Text style={styles.userNamePost}>{userName}</Text>
        <UserAvatar name={userName} size={50} style={styles.imgPost} />
      </View>
      {editMode ? <Input placeholder="قم بتعديل منشورك..." onChangeText={val => setNewContent(val)} /> : <Text style={styles.main}> {content}</Text>}
      {
        show ?
          <View style={styles.footer}>
            <Ripple
              onPress={() => setreviewMode(true)}
              rippleColor='rgb(255,255,255)'
              rippleOpacity={.5}
              style={styles.ripple}>
              <Icon name='rate-review' size={25} color='#F9A826' />
              <Text style={{ color: '#F9A826' }}>تقييم</Text>
            </Ripple>
            <Ripple
              onPress={() => setShowDial(!showDial)}
              rippleColor='rgb(255,255,255)'
              rippleOpacity={.5}
              style={styles.ripple}>
              {!showDial ?
                <>
                  <Icon name='call' size={25} color='#F9A826' />
                  <Text style={{ color: '#F9A826' }}>تواصل</Text>
                </>
                :
                <><Icon name='close' size={25} color='#F9A826' />
                  <Text style={{ color: '#F9A826' }}>إغلاق</Text>
                </>}
            </Ripple>
          </View> : editMode ? <View style={styles.footer}>
            <Ripple
              rippleColor='rgb(255,255,255)'
              onPress={newContent.length < 20 ? () =>
                ToastAndroid.show("20 حرف على الأفل", ToastAndroid.SHORT) : () => update(postID, newContent, setEditMode(false))}
              rippleOpacity={.5}
              style={styles.ripple}>
              <Icon name='thumb-up' size={25} color='#F9A826' />
              <Text style={{ color: '#F9A826' }}>حفظ</Text>
            </Ripple>
            <Ripple
              onPress={() => setEditMode(false)}
              rippleColor='rgb(255,255,255)'
              rippleOpacity={.5}
              style={styles.ripple}>
              <Icon name='thumb-down' size={25} color='#F9A826' />
              <Text style={{ color: '#F9A826' }} >إالغاء</Text>
            </Ripple>
          </View> : <View style={styles.footer}>
              <Ripple
                rippleColor='rgb(255,255,255)'
                onPress={() => setEditMode(true)}
                rippleOpacity={.5}
                style={styles.ripple}>
                <Icon name='edit' size={25} color='#F9A826' />
                <Text style={{ color: '#F9A826' }}>تعديل</Text>
              </Ripple>
              <Ripple
                onPress={() => del(postID)}
                rippleColor='rgb(255,255,255)'
                rippleOpacity={.5}
                style={styles.ripple}>
                <Icon name='delete' size={25} color='#F9A826' />
                <Text style={{ color: '#F9A826' }} >حذف</Text>
              </Ripple>
            </View>
      }
    </View >
  )
}
export default React.memo(Post)