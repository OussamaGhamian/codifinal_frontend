import React, { useState } from 'react'
import { View, Text, Dimensions } from 'react-native'
import Input from '../Input/Input'
import * as Animatable from 'react-native-animatable'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import StarRating from 'react-native-star-rating';

export default function AddReview({ close }) {
  const [comment, setComment] = useState('')
  const [starCount, setStarCount] = useState(0)
  return (
    <Animatable.View animation='zoomIn' duration={200} style={{ flex: 1, minHeight: Dimensions.get('window').height / 3, backgroundColor: 'white', justifyContent: 'space-around', alignItems: 'center', position: 'absolute', zIndex: 1, width: Dimensions.get('window').width - 40, borderLeftWidth: 5, borderColor: '#10153B', }}>
      <Icon name='close' size={30} onPress={() => close(false)} style={{ alignSelf: 'flex-end' }} />

      <StarRating
        disabled={false}
        starStyle={{ color: '#F9A826', marginHorizontal: 7 }}
        maxStars={5}
        rating={starCount}
        selectedStar={(rating) => setStarCount(rating)}
      />
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <Input placeholder='اترك تعليقك هنا...' onChangeText={val => setComment(val)} />
        <Icon name="send" size={40} color="#F9A826" onPress={() => console.log(starCount, comment, close(false))} />
      </View>
    </Animatable.View>
  )
}
