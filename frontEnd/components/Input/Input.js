import React from 'react'
import { TextInput, Dimensions } from 'react-native'

export default function Input({ onChangeText, onBlur, value, placeholder, keyboardType = 'default' }) {
  return (
    <TextInput
      style={{
        width: Dimensions.get('screen').width - 90,
        borderBottomWidth: 2.5,
        borderBottomColor: '#10153B',
        fontSize: 20,
        textAlign: 'right',
        marginBottom: 5,
      }}
      maxLength={110}
      keyboardType={keyboardType}
      onChangeText={onChangeText}
      onBlur={onBlur}
      value={value}
      placeholder={placeholder}
    />
  )
}
