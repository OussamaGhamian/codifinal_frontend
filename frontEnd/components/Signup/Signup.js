import React, { useEffect, useState } from 'react'
import { View, Dimensions, StyleSheet, Text, Image } from 'react-native'
import { Picker } from '@react-native-community/picker'
import Button from '../Button/Button'
import { Formik } from 'formik'
// import axios from 'axios' to be use in case fetch dont work again.
import * as Yup from 'yup'
import Input from '../Input/Input'
import { ScrollView } from 'react-native-gesture-handler'
import { Welcome } from '../../assets/images'

const Signup = ({ navigation }) => {
  const [country, setCountry] = useState('SY')
  const [city, setCity] = useState('city')
  const [cities, setCities] = useState([]);
  const [enabled, setEnabled] = useState(false)
  const validationSchema = Yup.object().shape({
    userName: Yup.string().required('خانة مطلوبة'),
    phoneNbr: Yup.mixed().required('خانة مطلوبة'),
    password: Yup.mixed().required('خانة مطلوبة')
  })
  const styles = StyleSheet.create({
    pickerContainer: { flexDirection: 'row', justifyContent: 'space-between', width: Dimensions.get('window').width - 80, marginVertical: 20 },
    picker: { height: 35, width: 140 },
    required: { color: 'red', alignSelf: "flex-end" }
  })
  const getCities = async () => {
    try {
      const response = await fetch(`https://countries-cities.p.rapidapi.com/location/country/${country}/city/list`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json;charset=UTF-8',
          "x-rapidapi-host": "countries-cities.p.rapidapi.com",
          "x-rapidapi-key": "e6a4767a59msh5102f1587a5da5fp1f1795jsn8cee6ea1f83e"
        }
      });
      const result = await response.json();
      if (result.status == 'success') {
        let arr = result.cities.map(city => city.name)
        setCities(arr)
        setEnabled(true)
      } else {
        console.log('fetching failed')
      }
    }
    catch (err) {
      console.log(`ERR: ${err}`)
    }
  }
  useEffect(() => {
    getCities()
  }, [country, setCountry])
  return (
    <ScrollView style={{ flex: 1, minHeight: Dimensions.get('window').height }} contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', }}>
      <Welcome height={250} />
      <Formik
        validationSchema={validationSchema}
        initialValues={{ userName: '', phoneNbr: '', password: '', country, city }}
        onSubmit={values => console.log(values, navigation.navigate('رمز التأكيد'))}
      >
        {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Input
              onChangeText={handleChange('userName')}
              onBlur={handleBlur('userName')}
              value={values.userName}
              placeholder='اسم المستخدم'
            />
            {errors.userName && touched.userName ? <Text style={styles.required}>{errors.userName}</Text> : <Text></Text>}
            <Input
              onChangeText={handleChange('phoneNbr')}
              onBlur={handleBlur('phoneNbr')}
              value={values.phoneNbr}
              keyboardType='phone-pad'
              placeholder='رقم الهاتف'
            />
            {errors.phoneNbr && touched.phoneNbr ? <Text style={styles.required}>{errors.phoneNbr}</Text> : <Text></Text>}
            <Input
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              value={values.password}
              placeholder='كلمة المرور'
            />
            {errors.password && touched.password ? <Text style={styles.required}>{errors.password}</Text> : <Text></Text>}
            <View style={styles.pickerContainer}>
              <Picker
                selectedValue={values.country}
                style={styles.picker}
                onValueChange={val => {
                  values.country = val
                  setCountry(val)
                  setCity('city')
                  values.city = 'city'
                  // console.log(country, cities)
                }}
              >
                <Picker.Item label="Syira" value="SY" />
                <Picker.Item label="Lebanon" value="LB" />
              </Picker>
              <Picker
                enabled={enabled}
                selectedValue={values.city}
                style={styles.picker}
                onValueChange={val => {
                  values.city = val
                  setCity(val)
                }}
              >
                <Picker.Item key={-1} label="city" value="city" />
                {cities.map((city, i) => <Picker.Item key={i} label={city} value={city} />)}
              </Picker>
            </View>
            <Button action={handleSubmit} text="أنشىء حسابك" active={(errors.userName == undefined && errors.phoneNbr == undefined && errors.password == undefined && values.city != 'city')} />
          </View>
        )}
      </Formik>
    </ScrollView>
  )
}
export default React.memo(Signup);