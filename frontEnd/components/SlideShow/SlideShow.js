import React, { useState, useRef } from 'react'
import { View, Text, Dimensions, StyleSheet } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Button from '../Button/Button'
import { Car, Medicine, Docs } from '../../assets/images'
import Ripple from 'react-native-material-ripple';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');
const styles = StyleSheet.create({
  slideContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'transparent'
  },
  banner: {
    fontSize: 25,
    marginTop: 50
  },
  skip: {
    color: 'grey',
    padding: 5,
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 20,
    alignItems: 'center',
    height: 30,
    width: 100,
    marginBottom: 10
  },
  buttonBox: {
    alignItems: 'center',
    marginTop: 'auto',
    marginBottom: 90,
  }
})
const SlideShow = ({ navigation }) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const carouselRef = useRef(null);
  const [carouselItems, setCarouselItems] = useState([
    {
      text: "مسافر ع الشام؟ جبلي علبة هالدوا",
    },
    {
      text: "طالع ع بيروت؟ خدني بطريقك",
    },
    {
      text: "وصلي هالورقة لو سمحت",
    }])

  const renderItem = ({ item, index }) => {
    return (
      <View style={styles.slideContainer} >
        <View>
          {index === 0 ? <Car height={210} /> : index === 1 ? <Medicine height={210} /> : <Docs height={220} />}
        </View>
        <Text style={styles.banner}>{item.text}</Text>
      </View>

    )
  }


  return (
    <View style={{ backgroundColor: 'white', flex: 1 }}>
      <Carousel
        layout={'stack'}
        layoutCardOffset={9}
        ref={carouselRef}
        data={carouselItems}
        sliderWidth={viewportWidth}
        itemWidth={viewportWidth}
        slideStyle={{ width: viewportWidth }}
        renderItem={renderItem}
        removeClippedSubviews
        decelerationRate='normal'
        onSnapToItem={activeIndex => setActiveIndex(activeIndex)} />
      <View style={styles.buttonBox}>
        <Button text={activeIndex === 2 ? 'انتقل للتطبيق' : "التالي"} action={activeIndex === 2 ? () => navigation.navigate('مرحبا') : () => carouselRef.current.snapToNext()} isRipple={true} />
        {activeIndex !== 2 ?
          <Ripple style={styles.skip} onPress={() => navigation.navigate('مرحبا')}><Text>تجاوز</Text></Ripple> : <Text style={styles.skip}></Text>}
      </View>
    </View>
  );
}


export default React.memo(SlideShow);