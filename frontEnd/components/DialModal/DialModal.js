import React from 'react'
import { View, Linking, Dimensions, Alert } from 'react-native'
import * as Animatable from 'react-native-animatable'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import Ripple from 'react-native-material-ripple'

export default function DialModal({ phoneNbr }) {
  return (
    <Animatable.View animation="pulse" easing="ease-in" iterationCount="infinite" style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', position: 'absolute', zIndex: 1, width: Dimensions.get('window').width - 40, height: 100, }}>
      {['whatsapp', 'phone', 'android-messages'].map((name, i) =>
        <Ripple
          onPress={() => i == 0 ?
            Linking.canOpenURL(`whatsapp://send?phone=${phoneNbr}`).then(supported => supported ? Linking.openURL(`whatsapp://send?phone=${phoneNbr}`) : Alert.alert('Alert',
              'WhatsApp is not installed'))
            :
            i == 1 ?
              Linking.openURL(`tel:${phoneNbr}`)
              :
              Linking.openURL(`sms:${phoneNbr}`)}
          rippleColor='gold'
          key={i}
          style={i % 2 == 0 ? { backgroundColor: '#10153B', borderRadius: 200 / 2, padding: 10, } : { backgroundColor: '#10153B', borderRadius: 200 / 2, padding: 10, alignSelf: 'flex-start' }}>
          <Animatable.View animation={i % 2 == 0 ? 'zoomInUp' : 'zoomInDown'} duration={1000}>
            <Icon name={name} size={40} color="#F9A826" />
          </Animatable.View>
        </Ripple>)}

    </Animatable.View>
  )
}
