import React, { useState } from 'react'
import { View, Text, Dimensions } from 'react-native'
import { Formik, } from 'formik'
import * as Yup from 'yup'
import Button from '../Button/Button'
import Input from '../Input/Input'
import { ScrollView } from 'react-native-gesture-handler'
import { Login } from '../../assets/images'
import Checkbox from '@react-native-community/checkbox'


const validationSchema = Yup.object().shape({
  phoneNbr: Yup.number("أدخل أرقام بين 0-9").min(8, "رقم هاتفك يبدو قصير").required('خانة مطلوبة'),
  password: Yup.mixed().required('خانة مطلوبة')
})

const Signin = ({ navigation, route }) => {
  // console.log(route.params.addToken)
  const [remember, setRemember] = useState(false)
  return (
    <ScrollView style={{ flex: 1, minHeight: Dimensions.get('window').height }} contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', }}>
      <Login height={250} />
      <Formik
        validationSchema={validationSchema}
        initialValues={{ phoneNbr: '', password: '' }}
        onSubmit={values => console.log(values, route.params.addToken(values.password))}
      >
        {({ handleChange, handleBlur, values, touched, errors, handleSubmit }) => (
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
            <Input
              onChangeText={handleChange('phoneNbr')}
              onBlur={handleBlur('phoneNbr')}
              placeholder='رقم الهاتف'
              keyboardType='phone-pad'
              value={values.phoneNbr}
            />
            {errors.phoneNbr && touched.phoneNbr ? <Text style={{ color: 'red', alignSelf: "flex-end" }}>{errors.phoneNbr}</Text> : <Text></Text>}
            <Input
              onChangeText={handleChange('password')}
              onBlur={handleBlur('password')}
              placeholder='كلمة المرور'
              value={values.password}
            />
            {errors.password && touched.password ? <Text style={{ color: 'red', alignSelf: "flex-end" }}>{errors.password}</Text> : <Text></Text>}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: Dimensions.get('window').width - 100, marginVertical: 59 }}>
              <Text style={{ textAlignVertical: 'center' }}>نسيت كلمة المرور</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Checkbox value={remember}
                  onValueChange={setRemember} />
                {/* {console.log(remember)} */}
                <Text>تذكرني</Text>
              </View>
            </View>
            <Button action={() => handleSubmit()} text='سجل دخول' active={errors.phoneNbr == undefined && errors.password == undefined} />
          </View>
        )}
      </Formik>
    </ScrollView>
  )
}
export default React.memo(Signin);