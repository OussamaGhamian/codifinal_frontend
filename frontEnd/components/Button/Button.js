import React from 'react'
import { TouchableOpacity, Text, StyleSheet, Dimensions, View } from 'react-native'
import Ripple from 'react-native-material-ripple';

const styles = StyleSheet.create({
  button: {
    borderRadius: 5,
    backgroundColor: '#10153B',
    padding: 7,
    width: Dimensions.get("window").width / 3,
  },
  btnText: {
    textAlign: "center",
    fontSize: 20,
    color: '#F9A826',
  },
  disabled: {

  }
})
export default function Button({ text, action, active = true }) {
  return (

    <>
      {active ?
        <Ripple
          onPress={action}
          rippleColor='rgb(255,255,255)'
          rippleOpacity={.5}
        >
            < TouchableOpacity style={styles.button} >
              <Text style={styles.btnText}>{text}</Text>
            </TouchableOpacity >
        </Ripple> :
        <TouchableOpacity disabled style={{ ...styles.button, backgroundColor: 'rgba(238, 238, 238, 5)', }} onPress={action} >
          <Text style={styles.btnText}>{text}</Text>
        </TouchableOpacity >
      }
    </>
  )
}
