import React from 'react'
import { View, Text, StyleSheet, Dimensions, Image } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import UserAvatar from 'react-native-user-avatar';

const styles = StyleSheet.create({
  postContainer: {
    width: Dimensions.get('window').width - 40,
    minHeight: Dimensions.get('window').height / 3.5,
    justifyContent: 'center',
    marginVertical: 20,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#eee',
    borderBottomWidth: 0,
    shadowColor: '#fff',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: .1,
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
    elevation: .8,
  },
  bannerDeco: {
    backgroundColor: '#10153B',
    position: 'absolute',
    top: 0,
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
    width: Dimensions.get('window').width - 40,
  }
  , header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    // paddingTop: 15
  },
  imgPost: {
    borderRadius: 100 / 2,
    width: 50,
    height: 50,
    borderColor: '#F9A826',
    borderWidth: 2.5,
    backgroundColor: '#10153B',
  },
  userNamePost: {
    marginLeft: 'auto',
    fontSize: 20,
    fontWeight: 'bold',
    paddingRight: 10,
    color: "#10153B"
  },
  datePost: {
    fontWeight: 'bold',
    color: '#F9A826',
  },
  main: {
    width: Dimensions.get('window').width - 60,
    maxHeight: Dimensions.get('window').height / 6,
    fontSize: 18,
    lineHeight: 28,
    textAlign: 'right',
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15
  }
});
export default function Review({ date, userName, comment, stars }) {
  const getStars = stars => {
    stars > 5 ? stars = 5 : null;
    stars < 1 ? stars = 1 : null;
    var indents = [];
    for (let index = 0; index < 5; index++) {
      stars > 0 ? indents.push(<Icon name='star' key={index} color='#F9A826' size={22} />) : indents.push(<Icon name='staro' key={index} color='#F9A826' size={22} />)
      stars--;
    }
    return indents
  }
  return (
    <View style={styles.postContainer}>
      <View style={styles.bannerDeco}><Text></Text></View>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ marginVertical: 'auto', justifyContent: 'center', paddingLeft: 5, flexDirection: 'column-reverse' }}>
          {getStars(stars)}
        </View>
        <View >
          <View style={styles.header}>
            <Text style={styles.datePost}>{date}</Text>
            <Text style={styles.userNamePost}>{userName}</Text>
            <UserAvatar name={userName} size={50} style={styles.imgPost} />
          </View>
          <Text style={styles.main}>{comment}</Text>
        </View>

      </View>
    </View>
  )
}
