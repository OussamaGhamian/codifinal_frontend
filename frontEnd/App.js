import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { SlideShow, Signup, Signin, } from './components'
import { Confirmation, SubmitCode, MyAccount, MyPosts, About, Requests, Offers } from './screens'
import AnimatedSplash from "react-native-animated-splash-screen";
import { NavigationContainer, } from '@react-navigation/native'
import { createStackNavigator, } from '@react-navigation/stack'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { AsyncStorage, SafeAreaView, StyleSheet, Text } from 'react-native'

// async () => {
//   let x = await AsyncStorage.getItem('isFirstTime') == null
//   console.log(x)
// }

const Stack = createStackNavigator();
const TopTab = createMaterialTopTabNavigator();
const BottomTap = createMaterialBottomTabNavigator();
const topTapOptions = {
  labelStyle: { fontSize: 16, color: '#F9A826', marginTop: 35 },
  style: { backgroundColor: "#10153B", },
  indicatorStyle: { backgroundColor: '#F9A826', }
}
const getIcon = (isFocus, iconName) => <Icon name={iconName} color={isFocus ? '#F9A826' : 'white'} size={24} />

const mainTopTaps = () => {
  return <TopTab.Navigator
    tabBarOptions={topTapOptions}
  >
    <TopTab.Screen name="اطلب المساعدة" component={Requests} />
    <TopTab.Screen name="منشوراتي" component={MyPosts} />
    <TopTab.Screen name="قدم المساعدة" component={Offers} />

  </TopTab.Navigator>
}
const mainBtmTaps = ({ route }) => {
  // console.log(`from mainBtm ${route.params.removeToken}`)
  return (
    <BottomTap.Navigator

      activeColor="#F9A826"
      style={{ backgroundColor: '#fff' }}
      shifting={true}
      initialRouteName='رئيسية'
      barStyle={{
        backgroundColor: '#10153B',
        borderTopWidth: 10,
        borderTopColor: '#10153B',
        borderTopEndRadius: 13,
        borderTopLeftRadius: 13,
        height: 60
      }}
    >
      <BottomTap.Screen name="حسابي" component={MyAccount} initialParams={{ logOut: route.params.removeToken }} options={{ tabBarIcon: ({ focused }) => getIcon(focused, 'account') }} />
      <BottomTap.Screen name="رئيسية" children={mainTopTaps} focused options={{ tabBarIcon: ({ focused }) => getIcon(focused, 'home') }} />
      <BottomTap.Screen name="حول" component={About} options={{ tabBarIcon: ({ focused }) => getIcon(focused, 'information') }} />
    </BottomTap.Navigator>
  )
}
const SignTabs = ({ route }) => {
  return (
    <TopTab.Navigator
      backBehavior={false}
      style={{ backgroundColor: 'white' }}
      tabBarOptions={topTapOptions}
    >
      <TopTab.Screen name="إنشاء حساب" component={Signup} />
      <TopTab.Screen name="تسجيل دخول" component={Signin} initialParams={{ addToken: route.params.addToken }} />
    </TopTab.Navigator>
  );
}
const App = () => {
  const [isloaded, setIsLoaded] = useState(false)
  const [isSignOut, setIsSignOut] = useState(false)
  const [userToken, setUserToken] = useState(null)

  useEffect(() => {
    setTimeout(() => {
      setIsLoaded(true)
    }, 3000);
  }, [])
  return (< AnimatedSplash
    translucent={true}
    isLoaded={isloaded}
    logoImage={require("./assets/images/logo.png")}
    backgroundColor={"#10153B"}
    logoHeight={500}
    logoWidht={500}
  >
    <SafeAreaView style={{ flex: 1, justifyContent: 'center', }}>
      <NavigationContainer >
        <Stack.Navigator
          screenOptions={{
            headerStyle: {
              backgroundColor: '#10153B',
            },
            headerTintColor: '#F9A826',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}
        >
          {userToken ?
            <>
              <Stack.Screen name="تطبيق" options={{ headerShown: false }} children={mainBtmTaps} initialParams={{ removeToken: () => setUserToken(null) }} />
            </>
            :
            <>
              {true ? <Stack.Screen name='لبيني ت لبيك' component={SlideShow} /> : null}
              <Stack.Screen name='مرحبا' options={{ headerShown: false }} component={SignTabs} initialParams={{ addToken: (token) => setUserToken(token) }} />
              <Stack.Screen name='رمز التأكيد' component={SubmitCode} />
              <Stack.Screen name='تأكيد حسابك' component={Confirmation} />
            </>}


        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  </AnimatedSplash >)
}
export default React.memo(App);