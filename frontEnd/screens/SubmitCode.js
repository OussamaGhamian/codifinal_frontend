import React, { useState } from 'react'
import { View } from 'react-native'
import { Message } from '../assets/images'
import { Button, Input } from '../components'
import * as Animatable from 'react-native-animatable';

export default function SubmmitCode({ navigation }) {
  const [code, setCode] = useState('')
  return (
    <Animatable.View animation="fadeInDown" style={{ flex: 1, justifyContent: 'space-evenly', alignItems: 'center' }}>
      <View style={{ borderRadius: 200, backgroundColor: '#10153B', padding: 40 }}>
        <Message width={150} height={150} />
      </View>
      <Input placeholder='أدخل رمز التأكيد' onChangeText={val => setCode(val)} />
      <Button text='أرسل' action={() => navigation.navigate('تأكيد حسابك', console.log(code))} />
    </Animatable.View>
  )
}
