import React, { useState } from 'react'
import { View, Text, StyleSheet, ScrollView, Dimensions } from 'react-native'
import { Review } from '../components'
import * as Animatable from 'react-native-animatable'
import UserAvatar from 'react-native-user-avatar';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
const width = Dimensions.get('window').width
const styles = StyleSheet.create({
  container: {
    flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', marginTop: 35
  },
  upperPart: {
    alignItems: 'center', justifyContent: 'center', borderBottomLeftRadius: 20, borderBottomRightRadius: 20,
    width
  },
  personalImg: { width: 150, height: 150, borderRadius: 100, borderColor: '#F9A826', borderWidth: 5, backgroundColor: '#10153B', },
  footerUpperPart: { flexDirection: 'row', width, justifyContent: 'space-evenly', backgroundColor: '#10153B', paddingTop: 73, borderBottomLeftRadius: 20, borderBottomRightRadius: 20, position: 'absolute', zIndex: -1, top: 125 }
  , footerText: {
    color: '#F9A826',
    width: width / 5,
    fontSize: 15,
    paddingBottom: 15
  }
})
export default function MyAccount({ route }) {
  // console.log(`from myAcc: ${route.params.logOut}`)
  const [showMenu, setShowMenu] = useState(false)
  const [reviews, setReviews] = useState([
    {
      id: 0,
      userName: 'أكرم أحمد',
      stars: 1,
      comment: 'شكرا لك لقد قدمت لي خدمة لن أنساها طول عمري',
      date: "02-02-2020"
    }, {
      id: 1,
      userName: 'أكرم أحمد',
      stars: 4,
      comment: 'شكرا لك لقد قدمت لي خدمة لن أنساها طول عمري',
      date: "02-02-2020"
    }, {
      id: 2,
      userName: 'أكرم أحمد',
      stars: 2,
      comment: 'شكرا لك لقد قدمت لي خدمة لن أنساها طول عمري',
      date: "02-02-2020"
    }, {
      id: 3,
      userName: 'أكرم أحمد',
      stars: 3,
      comment: 'شكرا لك لقد قدمت ليخدمة لن أنساها طdsfdsfsdfsdfsdfdssdfsdf  خدمةخدمة لن أنسها طول عمري  خدمة لن أنساها طول عمري ',
      date: "02-02-2020"
    }
  ])
  return (
    <View style={styles.container}>
      <Animatable.View
        animation="fadeInDown" style={styles.upperPart}>
        <View style={{ alignSelf: 'flex-start', }} >
          {
            showMenu ?
              <Animatable.View animation='fadeInLeft' duration={200} style={{ flexDirection: 'row', justifyContent: 'space-between', width: 160 }}>
                <Icon name='close-circle-outline' size={35} color='#10153B' onPress={() => setShowMenu(false)} />
                <Icon name='logout' size={35} color='#10153B' onPress={() => route.params.logOut()} />
                <Icon name='account-edit' size={35} color='#10153B' onPress={() => console.log('editing profile')} />
              </Animatable.View>
              :
              <Icon name='dots-vertical-circle-outline' size={35} color='#10153B' onPress={() => setShowMenu(true)} />
          }
        </View>
        <UserAvatar name='سمر الفطايري' size={50} style={styles.personalImg} />
        <View style={styles.footerUpperPart}>
          <Text style={styles.footerText}>76664873</Text>
          <Text style={styles.footerText}>سمر الفطايري</Text>
          <Text style={styles.footerText}>دمشق</Text>
        </View>
      </Animatable.View>
      <ScrollView style={{ flex: 1, marginTop: 50 }}
        showsVerticalScrollIndicator={false} contentContainerStyle={{ justifyContent: 'center', alignItems: 'center' }} >
        <Text>عدد التعليقات {reviews.length}</Text>
        {
          reviews.map(review =>
            <Review
              userName={review.userName}
              comment={review.comment}
              date={review.date}
              stars={review.stars}
              key={review.id} />
          )
        }
      </ScrollView>
    </View>
  )
}
