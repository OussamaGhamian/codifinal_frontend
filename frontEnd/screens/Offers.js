import React, { useState } from 'react'
import { View, Text, ScrollView } from 'react-native'
import { Post, Send } from '../components'
import * as Animatable from 'react-native-animatable'

export default function Offers() {
  const [offers, setOffers] = useState([
    {
      id: '1',
      userName: 'فادي الحميد',
      date: '02-02-2020',
      phoneNbr: 96176664873,
      content: 'أنا مسافر خلال يومين الى حلب من يريد أن أجلب له شيء معي صغير الحجم فليتواصل معي'
    },
    {
      id: '1',
      userName: 'فادي الحميد',
      date: '02-02-2020',
      phoneNbr: 76664873,
      content: 'أنا مسافر خلال يومين الى حلب من يريد أن أجلب له شيء معي صغير الحجم فليتواصل معي'
    },
    {
      id: '2',
      userName: 'فادي الحميد',
      date: '02-02-2020',
      phoneNbr: 76664873,
      content: 'أنا مسافر خلال يومين الى حلب من يريد أن أجلب له شيء معي صغير الحجم فليتواصل معي'
    },
    {
      id: '3',
      userName: 'فادي الحميد',
      date: '02-02-2020',
      phoneNbr: 76664873,
      content: 'أنا مسافر خلال يومين الى حلب من يريد أن أجلب له شيء معي صغير الحجم فليتواصل معي'
    },
  ])
  const addOffer = (newOffer) => setOffers([newOffer, ...offers])
  return (
    <View style={{
      flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "white"
    }}>
      <ScrollView showsVerticalScrollIndicator={false} >
        {offers.map((offer, index) =>
          <Post userName={offer.userName} date={offer.date} content={offer.content} phoneNbr={offer.phoneNbr} key={index} />
        )}
      </ScrollView>
      <Send addOffer={addOffer} />
    </ View>
  )
}
