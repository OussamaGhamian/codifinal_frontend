import React, { useState } from 'react'
import { View, ScrollView, Alert, ToastAndroid } from 'react-native'
import { Post, } from '../components'

export default function MyPosts() {
  const [posts, setPosts] = useState([
    {
      id: '0',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '1',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '2',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '3',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
  ])
  const del = id => {
    Alert.alert("حذف",
      "هل تريد حذف منشورك",
      [
        {
          text: "إلغاء",
          style: "cancel"
        },
        { text: "حذف", onPress: () => setPosts(posts.filter(post => post.id !== id), ToastAndroid.show("تم حذف منشورك", ToastAndroid.SHORT)) }
      ],
      { cancelable: false }
    )
  };
  const update = (id, content) => {
    let arr = posts.map(post => {
      if (post.id === id) {
        post.content = content
      }
      return post
    })
    setPosts(arr)
  }
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "white" }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {posts.map((post, index) => <Post userName={post.userName} postID={post.id} date={post.date} content={post.content} key={index} phoneNbr={post.phoneNbr} show={false} del={del} update={update} />)}
      </ScrollView>
    </View>
  )
}
