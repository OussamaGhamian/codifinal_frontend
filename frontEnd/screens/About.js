import React from 'react'
import { View, Text, StyleSheet, Dimensions, Linking, Alert } from 'react-native'
import { ME } from '../assets/images'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    margin: 0,
    backgroundColor: '#fff'
  },
  img: {
    backgroundColor: 'rgba(0,0,255,.2)',
  },
  text: {
    fontSize: 18,
    marginVertical: 30,
    fontWeight: 'bold',
    lineHeight: 35,
    padding: 20,
    borderLeftWidth: 15,
    borderLeftColor: '#10153B',
  },
  footer: {
    width: Dimensions.get('window').width,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRightWidth: 15,
    borderRightColor: '#10153B',

  },
  odd: {
    backgroundColor: '#F9A826',
    borderRadius: 100,
    borderWidth: 3,
    borderColor: '#10153B',
    padding: 5
  },
  even: {
    backgroundColor: '#10153B',
    borderRadius: 100,
    borderWidth: 3,
    borderColor: '#F9A826',
    padding: 5
  }
})
export default function About() {
  return (
    <View style={styles.container}>
      <View style={styles.img}>
        <ME height={Dimensions.get('window').height / 3} />
      </View>
      <Text style={styles.text}>
        هذا التطبيق تم تطويره من قبل مهندس البرمجيات <Text style={{ color: '#F9A826', backgroundColor: '#10153B', }}>أسامة الغميان</Text> بهدف تسهيل بعض العقبات التي يواجهها القاطنين في سوريا أو لبنان جراء عدم امكانية التنقل بسهولة أو بسبب الكلفة الكبيرة.هذا التطبيق يعتمد أسس التكافل الاجتماعي بين الأفراد لتقديم خدمة الشحن الغير مدفوعة بين البلدين.
      </Text>
      <View style={styles.footer}>
        <View style={styles.odd}><Icon name='web' size={40} color="#10153B" onPress={() => Linking.openURL(`https://oussamaghamian.netlify.app/`)} /></View>
        <View style={styles.even}><Icon name='phone' size={40} color="#F9A826" onPress={() => Linking.canOpenURL(`whatsapp://send?phone=96176664873`).then(supported => supported ? Linking.openURL(`whatsapp://send?phone=96176664873`) : Alert.alert('Alert',
          'WhatsApp is not installed'))} /></View>
      </View>
    </View>
  )
}
