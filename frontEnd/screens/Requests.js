import React, { useState } from 'react'
import { View, ScrollView } from 'react-native'
import { Post, Send } from '../components'

export default function Requests() {
  const [requests, setRequests] = useState([
    {
      id: '1',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '1',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '2',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
    {
      id: '3',
      userName: 'فادي الحميد',
      phoneNbr: '76664873',
      date: '02-02-2020',
      content: 'أريد المساعدة في احضار علبة دواء من دمشق اسم الدواء باراسيتامول 50 ملغ في خلال اسبوعين لحالة صحية طارئة'
    },
  ])
  const addRequest = (newRequest) => setRequests([newRequest, ...requests])
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: "white" }}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {requests.map((request, index) => <Post userName={request.userName} date={request.date} content={request.content} key={index} phoneNbr={request.phoneNbr} />)}
      </ScrollView>
      <Send addRequest={addRequest} />
    </View>
  )
}
