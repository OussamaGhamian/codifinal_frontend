import React from 'react'
import { View } from 'react-native'
import { Confirm } from '../assets/images'
import { Button } from '../components'
import * as Animatable from 'react-native-animatable';

export default function Confirmation({navigation}) {
  return (
    <Animatable.View animation="fadeInDown" style={{ flex: 1, justifyContent: 'space-evenly', alignItems: 'center' }}>
      <View style={{ borderRadius: 200, backgroundColor: '#10153B', padding: 40 }}>
        <Confirm width={150} height={150} />
      </View>
      <Button text='تم' action={() => navigation.navigate('تسجيل دخول')} />
    </Animatable.View>
  )
}
